import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
   
    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";
	listaPessoas.add(umaPessoa);
	return mensagem;
    }



    public void remover(Pessoa umPessoa) {
        listaPessoas.remove(umPessoa);
    }
    
    public Pessoa pesquisar( String nome){
		for(Pessoa umaPessoa: listaPessoas){
			if( umaPessoa.getNome().equalsIgnoreCase(nome)) return umaPessoa;
		}
		return null;
	}

	public void getListaPessoas(){
		for(Pessoa umaPessoa: listaPessoas){
                    System.out.println( umaPessoa.getNome() + (" ") + umaPessoa.getTelefone());
                }                        
	}
}
    
   