import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objeto Lista
    ControlePessoa umControle = new ControlePessoa();
    
    //Bem-vindo � Agenda
    System.out.println(" ** Agenda ** ");
    System.out.println("Para sair digite -1");
    System.out.println("");
    
    for(int count = 0; count<50; count++){ 
        
        //Varios objetos pessoa precisam ser criado porque cada objeto ter�
        //um nome diferente e um telefone diferente
        //Entretanto a Lista ser� apenas uma: Agenda
        Pessoa umaPessoa = new Pessoa();   
        
        //Mensagem de pedido de entrada
        System.out.println("Digite o nome da Pessoa:");
        entradaTeclado = leitorEntrada.readLine();
        String umNome = entradaTeclado;
        umaPessoa.setNome(umNome);
        if(entradaTeclado.equalsIgnoreCase("-1"))
            break;
        
        System.out.println("Digite o telefone da Pessoa:");
        entradaTeclado = leitorEntrada.readLine();
        String umTelefone = entradaTeclado;
        umaPessoa.setTelefone(umTelefone);
        
        if(entradaTeclado.equalsIgnoreCase("-1"))
            break;
        
        //adicionando uma pessoa na lista de pessoas do sistema
        umControle.adicionar(umaPessoa);
    }
    
    
    //Mostrar pessoas cadastradas � lista
    System.out.println("Pessoas cadastradas:");
    System.out.println("=================================");
    umControle.getListaPessoas();
    System.out.println("=================================");
    System.out.println("Tenha um Bom dia!");
    
    

  }

}